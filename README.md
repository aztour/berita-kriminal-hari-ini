# berita kriminal hari ini dari kriminologi.id

![kriminologi.id](https://kriminologi.id/assets/website/images/kriminologi_c.png)

Kriminologi.id merupakan media yang menyajikan [berita kriminal hari ini](https://kriminologi.id) terbaru dan terupdate 
disajikan secara mendalam dan dikupas dari sudut pandang jurnalistik. Dengan motto membaca fenomena kejahatan diharapkan
dapat menjadi sumber berita kriminal oleh para pembaca.

Kriminologi.id diharapkan menjadi pengetahuan dan referensi di masa depan dengan menyediakan rubrikasi yang 
menyangkut hampir seluruh unsur dan aspek kejahatan. Kami mengundang anda untuk berpatisipasi dalam 
meningkatkan media pemberitaan yang lebih baik lagi.

Berita yang termasuk ke dalam berita kejahatan adalah pembunuhan, penipuan, pemerkosaan, pencopetan, 
pencurian, perampokan, narkoba, tawuran, penganiayaan dan sebagainya yang melanggar hukum.Dimana 
dan kapan saja, berita kriminal mampu menarik perhatian khalayak untuk mencari tahu apa yang 
terjadi di sekitar kita.

Kriminologi.id memberikan kesempatan kepada pembaca untuk saling berbagi pengalaman 
dan pengetahuan melalui kolom komentar.
